package test;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.*;

import com.onshop.tracer.Log;
import com.sun.xml.internal.ws.api.Component;

import com.onshop.tracer.*;

public class kFrame extends JFrame
{
	private static int count = 0;
	
	private kFrame()
	{
		JMenuBar mb = new JMenuBar();
		
		JMenu  mn = new JMenu("File");
		JMenu  mh = new JMenu("Help");

		JMenu  ma = new JMenu("Add");

		ma.add( new JMenuItem(new AbstractAction("Amazon") {
			public void actionPerformed(ActionEvent e) {
		        onFileAddAmazon();
		    }
		}));		
		
		mn.add(ma);
		
		mb.add(mn);
		mb.add(mh);
		
		JMenuItem mie = new JMenuItem("Exit");
		
		mie.addActionListener(new java.awt.event.ActionListener() {
	    @Override
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
        Log.log("Action for close frame.");
        
        dispose();
	    }			
		});
		
		mn.add(mie);
		
		setJMenuBar(mb);
		
		setSize(800, 600);
		
		setLayout(null);
		
		setVisible(true);

		addWindowListener(new java.awt.event.WindowAdapter() {
	    @Override
	    public void windowClosing(java.awt.event.WindowEvent e) {
	        e.getWindow().dispose();
	        Log.log("Main frame closed!");
	    }
		});
	}
	
	public static kFrame getFrame()
	{
		if (count > 0)
			return null;
		
		kFrame fr = new kFrame();
		
		count = 1;
		
		return fr;
	}
	
	public void onFileAddAmazon()
	{
		String url = kUtils.getInputText(this, "Input product id or URL");
		
		IPriceTraser tracer = new PricerAmazon();
		
		PriceItem item = tracer.checkItemSource(url);
	}
}
