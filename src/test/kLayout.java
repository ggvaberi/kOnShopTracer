package test;

import com.onshop.tracer.*;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class kLayout extends JFrame  implements ActionListener
{
	private JFrame m_parent = null;
	
	public kLayout(JFrame parent)
	{
		super();
		
		m_parent = parent;
	}
	
	public boolean on()
	{
		if (m_parent == null)
			return false;
		
		Dimension dm = m_parent.getSize();
		
		this.setSize(dm);
		
		m_parent.add(this);
		
		return true;
	}
	
	public boolean off()
	{
		if (m_parent == null)
			return false;
		
		m_parent.remove(this);
		
		return true;
	}
	
	public void actionPerformed(ActionEvent e) 
	{    
		Object source = e.getSource();
		String command = e.getActionCommand(); 
		
		Log.log(" Action command: " + command);
		if (source == m_parent)
		{
			
		}
	}
}
