package test;

import java.awt.Component;

import javax.swing.JOptionPane;

public class kUtils
{
	public static String getInputText(Component par, String info)
	{
		String text = null;
		
		try 
		{
			text = JOptionPane.showInputDialog(par, info);
		} 
		catch (Exception e) 
		{
			
		}
		
		return text;
	}
}
