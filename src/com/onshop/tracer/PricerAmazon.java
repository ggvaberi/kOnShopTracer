package com.onshop.tracer;

import java.io.File;
import java.io.IOException;
 
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PricerAmazon implements IPriceTraser
{

	public PricerAmazon()
	{
		
	}
	
	public PriceItem checkItemSource(String src)
	{
		PriceItem item = null;

		if (src.length() < 1)
			return null;

		String https = src.substring(0, 8);
		
		if (https.compareTo("https://") != 0)
			return null;
		
		Document document;
		
    try {
        document = Jsoup.connect(src).userAgent("Mozilla/5.0").get();
        Elements question = document.select("#price");
        System.out.println("Price is " + question.html());
    } catch (IOException e) {
        e.printStackTrace();
    }
    
		return item;
	}
}
